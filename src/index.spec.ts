import WordChain, { isPass, ResultType } from "./index";

beforeEach(() => {
  jest.spyOn(global.Math, "random").mockReturnValue(0);
});

afterEach(() => {
  jest.spyOn(global.Math, "random").mockRestore();
});

describe("WordChain.start", () => {
  const dictionary = ["a", "b", "c"];
  const wordChain = new WordChain(dictionary);

  const result = wordChain.start();

  test("should return a random word from the input array", () => {
    expect(dictionary).toContain(result.word);
  });

  test("should pass", () => {
    expect(result.resultType).toBe(ResultType.PASS);
  });

  test("should return a score of zero", () => {
    expect(result.score).toBe(0);
  });

  test("should throw an error if invoked more than once", () => {
    expect(() => wordChain.start()).toThrow();
  });
});

describe("WordChain.play", () => {
  test("should return a random word from the input array when a valid word is played", () => {
    const dictionary = ["ab", "bc", "ca"];
    const wordChain = new WordChain(dictionary);

    wordChain.start();
    const result = wordChain.play("bc");

    if (isPass(result)) {
      expect(result.word).toBe("ca");
    } else {
      fail("Result was not pass");
    }
  });

  test("should not return a previous word from the input array when a valid word is played", () => {
    const dictionary = ["ab", "ac", "ba"];
    const wordChain = new WordChain(dictionary);

    wordChain.start();
    const result = wordChain.play("ba");

    if (isPass(result)) {
      expect(result.word).toBe("ac");
    } else {
      fail("Result was not pass");
    }
  });

  test("should fail when reusing a previous supplied word", () => {
    const dictionary = ["aa", "ab"];
    const wordChain = new WordChain(dictionary);

    wordChain.start();
    const result = wordChain.play("aa");

    expect(result.resultType).toBe(ResultType.FAIL);
  });

  test("should fail when reusing a previous answered word", () => {
    const dictionary = ["ab", "bc", "cb", "bd"];
    const wordChain = new WordChain(dictionary);

    wordChain.start();
    wordChain.play("bc");
    const result = wordChain.play("bc");

    expect(result.resultType).toBe(ResultType.FAIL);
  });

  test("should fail when the word is not in the dictionary", () => {
    const dictionary = ["ab", "bc"];
    const wordChain = new WordChain(dictionary);

    wordChain.start();
    const result = wordChain.play("bd");

    expect(result.resultType).toBe(ResultType.FAIL);
  });

  test("should fail when the first character of the word does not match the last character of the previous word from start", () => {
    const dictionary = ["ab", "bc", "ca"];
    const wordChain = new WordChain(dictionary);

    wordChain.start();
    const result = wordChain.play("ca");

    expect(result.resultType).toBe(ResultType.FAIL);
  });

  test("should fail when the first character of the word does not match the last character of the previous word from play", () => {
    const dictionary = ["ab", "bc", "cd", "de", "ef"];
    const wordChain = new WordChain(dictionary);

    wordChain.start();
    wordChain.play("bc");
    const result = wordChain.play("ef");

    expect(result.resultType).toBe(ResultType.FAIL);
  });

  test("should notify end when no words can be returned for the answer", () => {
    const dictionary = ["ab", "bc"];
    const wordChain = new WordChain(dictionary);

    wordChain.start();
    const result = wordChain.play("bc");

    expect(result).toStrictEqual({ resultType: ResultType.END, score: 1 });
  });

  test("should return a score equal to the number of successful plays when a valid word is played", () => {
    const dictionary = ["ab", "bc", "cd", "de", "ef"];
    const wordChain = new WordChain(dictionary);

    wordChain.start();
    wordChain.play("bc");
    const result = wordChain.play("de");

    expect(result.score).toBe(2);
  });

  test("should return a score equal to the number of successful plays when a invalid word is played", () => {
    const dictionary = ["ab", "bc", "cd", "de", "ef", "fg"];
    const wordChain = new WordChain(dictionary);

    wordChain.start();
    wordChain.play("bc");
    wordChain.play("de");
    const result = wordChain.play("hi");

    expect(result.score).toBe(2);
  });

  test("should return a score equal to the number of successful plays when an end word is played", () => {
    const dictionary = ["ab", "bc", "cd", "de"];
    const wordChain = new WordChain(dictionary);

    wordChain.start();
    wordChain.play("bc");
    const result = wordChain.play("de");

    expect(result.score).toBe(2);
  });
});

describe("WordChain constructor", () => {
  test("should require a dictionary with at least one item", () => {
    expect(() => new WordChain([])).toThrow();
  });
});
